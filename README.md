# JobProcessing

Current efforts have involved exploring various processing options for CC jobs.  A couple tools being considered are:

* Gearman
* Sidekiq

This repo includes some code that can be used to read data from a database (MySql by default) and write to a MongoDB instance using Gearman job servers, workers and clients.


## Requisites

* Mongo must be installed locally and should be up and running.
* Gearman must be installed locally; a job server should be started

Install MongoDB

```bash
brew update
brew install mongo
mongod
```

Install Gearman

```bash
brew update
brew install gearman
gearmand --verbose DEBUG -p 4730
```


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'job_processing'
```

And then install gem:

```bash
bundle
```

Or install it yourself as:

```bash
gem install job_processing
```

And then setup:

```bash
rake job_processing:setup
```

Follow the directions from the post setup.

## Configuration

Provide the desired configuration for the source database and the destination mongo database.

```ruby
  # ./config/configatron/job_processing.rb

  ...

  configatron.logging do |logging|
    logging.logger  =  Logger.new(STDOUT)
    logging.level   =  :debug
  end
  
  configatron.srcdb do |srcdb|
    srcdb.username  =  ''
    srcdb.password  =  ''
    srcdb.host      =  'localhost'
    srcdb.port      =  3306
    srcdb.database  =  'crystal_development'

    # srcdb.uri       =  'mysql://username:password@localhost:3306/crystal_development'
  end

  configatron.mongo do |mongo|
    mongo.uri       = 'mongodb://localhost:27017/test'
  end

  ...
```


## Usage

An instance of the `MongoDB` server should have already been started in a terminal window.  If it hasn't, start it up now.  Make sure your source database is running and tables are populated.  Also, make sure that at least one `Gearman` job server is running.

In a different terminal, run the example.  This example does not actually use `Gearman` to utilize workers and clients.

```bash
ruby example.rb
```

This example *does* utilize workers and clients.  Rake tasks have been created to start up the workers and run clients.  Indicate how many records should be submitted to Mongo in a given request using the `--bcount` option.

```bash
rake job_processing:worker:start
rake job_processing:client:run -- --job_servers=localhost:4730 --bcount=1000
```

Use the Mongo console to check on the results.

```bash
mongo

use development
show collections
db.job_processing_worker_inventories.count()

# clean out the collection; garbage collect
db.job_processing_worker_inventories.drop()
db.repairDatabase()
```


## Troubleshooting

When using MongoDB locally, you may see a warning like this when starting up the mongo console (not the mongo server):

> 2015-07-27T08:04:52.297-0700 I CONTROL  [initandlisten] ** WARNING: soft rlimits too low. Number of files is 256, should be at least 1000

It has something to do with memory limits.  Adjust those limits in the same terminal that you will be running `mongod` from and then restart `mongod`.

```bash
ulimit -n 2048
mongod
```


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake rspec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).


## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/job_processing. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

