# Benchmarking

Gearman was one of the options considered when trying to find a good job processing solution for the CC application.  After installing locally, I ran some benchmark tests with the following results.  This is probably not very telling since everything was running on my local development machine.  No doubt, the numbers should look different when going across the network to access a remote Mongo instance.

```
#!shell

rake job_processing:client:run -- --job_servers=localhost:4730 --bcount=2000
23015 records will be processed
Client will request processing a max of 2000 records per batch
{"iCount":23015,"time":2.07}
{"iCount":23015,"time":1.26}
{"iCount":23015,"time":1.23}
{"iCount":23015,"time":1.22}

rake job_processing:client:run -- --job_servers=localhost:4730 --bcount=1000
23015 records will be processed
Client will request processing a max of 1000 records per batch
{"iCount":23015,"time":1.22}
{"iCount":23015,"time":1.23}
{"iCount":23015,"time":1.25}
{"iCount":23015,"time":1.25}

rake job_processing:client:run -- --job_servers=localhost:4730 --bcount=750
23015 records will be processed
Client will request processing a max of 750 records per batch
{"iCount":23015,"time":3.02}
{"iCount":23015,"time":1.24}
{"iCount":23015,"time":1.23}
{"iCount":23015,"time":1.28}

rake job_processing:client:run -- --job_servers=localhost:4730 --bcount=5000
23015 records will be processed
Client will request processing a max of 5000 records per batch
{"iCount":23015,"time":1.76}
{"iCount":23015,"time":1.44}
{"iCount":23015,"time":1.39}
{"iCount":23015,"time":1.4}

rake job_processing:client:run -- --job_servers=localhost:4730 --bcount=500
23015 records will be processed
Client will request processing a max of 5000 records per batch
{"iCount":23015,"time":4.03}
{"iCount":23015,"time":1.25}
{"iCount":23015,"time":3.54}
{"iCount":23015,"time":1.26}
{"iCount":23015,"time":1.26}
```


