require File.join(File.dirname(File.expand_path(__FILE__)), 'colors')

include Colors

desc "attempt the setup procedures"
task job_processing: ["job_processing:setup"]

namespace :job_processing do
  desc "attempt the setup procedures"
  task setup: ["setup:copy:attempt"]

  namespace :setup do
    task :already_setup do
      if Dir.exists?("config")
        puts red "it looks like you may already have set things up (try 'job_processing:setup:force' if you wish to over-write)"
        fail
      end
    end

    desc "attempt the setup procedures"
    task attempt: ["copy:attempt"]

    desc "force the setup procedures"
    task force: ["copy:force"]

    desc "attempt a copy the config files"
    task copy: ["copy:attempt"]

    namespace :copy do
      forced=false

      desc "attempt a copy the config files"
      task :attempt do
        root_dir = Dir.pwd
        file_mod_cnt = 0

        FileUtils.mkdir "config" unless Dir.exists?("config")

        def check_file(file, forced, &blk)
          overwrite = false
          file_exists = true

          if(!forced)
            file_exists = File.exists?(file)
            if file_exists
              puts yellow "it looks like #{file} already exists; over-write? (Y/n)"
              input = STDIN.gets.strip
              if input == 'Y'
                overwrite = true
              end
            end
          end

          res = if(forced or !file_exists or overwrite)
            blk.call
            puts green "file copied: #{file}"
            1
          else
            0
          end

          res
        end

        gem_base_path = '/Users/davidvezzani/reliacode/crystal_commerce/job_processing'

        # source = File.join(Gem.loaded_specs["job_processing"].full_gem_path, "config", "database.yml.sample")
        source = File.join(gem_base_path, "config", "database.yml.sample")
        target = File.join(root_dir, "config", "database.yml")
        file_mod_cnt += check_file(target, forced){ FileUtils.cp_r source, target }

        # source = File.join(Gem.loaded_specs["job_processing"].full_gem_path, "config", "mongoid.yml.sample")
        source = File.join(gem_base_path, "config", "mongoid.yml.sample")
        target = File.join(root_dir, "config", "mongoid.yml")
        file_mod_cnt += check_file(target, forced){ FileUtils.cp_r source, target }

        source = File.join(gem_base_path, "config", "configatron", "job_processing.rb.sample")
        target = File.join(root_dir, "config", "configatron", "job_processing.rb")
        file_mod_cnt += check_file(target, forced){ FileUtils.cp_r source, target }
        
        source = File.join(gem_base_path, "templates", "example.rb")
        target = File.join(root_dir, "example.rb")
        file_mod_cnt += check_file(target, forced){ FileUtils.cp_r source, target }
        
        
        puts blue "#{file_mod_cnt} file(s) copied"
        Rake::Task["job_processing:setup:info"].invoke
      end

      desc "force a copy the config files"
      task :force do
        forced=true
        # Rake::Task["force"].execute
        Rake::Task["job_processing:setup:copy:attempt"].invoke
      end
    end

    desc "display post setup message"
    task :info do
      puts blue "
You will need to tweak some configuration, providing 
accurate credential information and mongo database config
in order for the example to run.

The 'example.rb' file has been created to facilitate a 
simple run.

mvim -p config/* example.rb
ruby example.rb
"
    end
  end
end

task :default => :job_processing


