require File.join(File.dirname(File.expand_path(__FILE__)), 'colors')

require 'bundler'
Bundler.require(:default)
require 'optparse'

include Colors

namespace :job_processing do
  
  task worker: ["worker:start"]
  namespace :worker do

    desc "number of records to be processed"
    task :rcount do
      cnt = JobProcessing::Worker::Sync.new.count
      puts green "#{cnt} records that need to be processed"
    end

    desc "stop worker(s)"
    task :stop do
      worker_pids_txt = File.join(Dir.pwd, "tmp", "worker_pids.txt")
      if(!File.exists?(worker_pids_txt))
        raise "tmp/worker_pids.txt does not exist; were workers ever started?"
      end

      worker_pids = IO.read(worker_pids_txt)
      worker_pids.split(/,/).each do |pid|
        Process.kill("TERM", pid.to_i)
        puts green "Killed worker ##{pid}"
      end

      File.delete worker_pids_txt
    end
    
    desc "start up worker(s)"
    task :start do
      worker_pids_txt = File.join(Dir.pwd, "tmp", "worker_pids.txt")
      if(File.exists?(worker_pids_txt))
        raise "tmp/worker_pids.txt exists; are workers already running?"
      end
      
      options = {}
      argv = ((ARGV.length > 2)) ? (ARGV[2..-1]) : []
      OptionParser.new() do |opts|
        opts.banner = "Usage: rake job_processing:worker:start [options]"
        opts.on("-w", "--wcount {integer}","number of workers to start up", Integer) do |wcount|
          options[:wcount] = wcount
        end
        opts.on("-b", "--bcount {integer}","number of records to be processed in each batch job", Integer) do |bcount|
          options[:bcount] = bcount
        end
        opts.on("-s", "--job_servers {string}","ip addresses of servers in job server pool (e.g., 'localhost:4730,localhost:4731')", String) do |job_servers|
          options[:job_servers] = job_servers
        end
      end.parse!(argv)

      number_of_workers = (options.has_key?(:wcount)) ? options[:wcount] : 1
      puts green "Starting #{number_of_workers} worker(s)"

      max_batch_size = (options.has_key?(:bcount)) ? options[:bcount] : "<all>"
      puts green "Workers will process a max of #{max_batch_size} records per batch"

      # require 'byebug'
      # debugger
      # rcount = JobProcessing::Worker::Sync.new.count
      # puts blue "#{rcount} records that need to be processed"

      JobProcessing::Worker::Sync.new.start(options)
      
      # worker_pids = []
      # (1..number_of_workers).each do |idx|
      #   worker_pids << fork do
      #     JobProcessing::Worker::Sync.new.start(options)
      #   end
      #   Process.detach(worker_pids.last)
      #
      #   puts green "Started worker ##{worker_pids.last}"
      # end
      #
      # tmp_dir = File.join(Dir.pwd, "tmp")
      # if(!File.exists?(tmp_dir))
      #   Dir.mkdir(tmp_dir)
      # end
      #
      # File.open(File.join(tmp_dir, "worker_pids.txt"), "w"){ |f|
      #   f.write worker_pids.join(",")
      # }

      puts green "Done starting worker(s)"
    end
  end  
  
  task client: ["client:run"]
  namespace :client do

    def process_alive?(pid)
      begin
        Process.getpgid( pid )
        true
      rescue Errno::ESRCH
        false
      end
    end

    desc "run a client"
    task :run do
      options = {}
      argv = ((ARGV.length > 2)) ? (ARGV[2..-1]) : []
      OptionParser.new() do |opts|
        opts.banner = "Usage: rake job_processing:client:run [options]"
        opts.on("-b", "--bcount {integer}","number of records to be processed in each batch job", Integer) do |bcount|
          options[:bcount] = bcount
        end
        opts.on("-c", "--ccount {integer}","number of clients in client pool to request batch job processing", Integer) do |ccount|
          options[:ccount] = ccount
        end
        opts.on("-s", "--job_servers {string}","ip addresses of servers in job server pool (e.g., 'localhost:4730,localhost:4731')", String) do |job_servers|
          options[:job_servers] = job_servers
        end
      end.parse!(argv)

      recordset_size = JobProcessing::Worker::Sync.new.count
      puts blue "#{recordset_size} records will be processed"

      max_batch_size = (options.has_key?(:bcount)) ? options[:bcount] : recordset_size
      puts blue "Client will request processing a max of #{max_batch_size} records per batch"

      # max_clients = (options.has_key?(:ccount)) ? options[:ccount] : 1
      # puts blue "Client pool consists of #{max_clients} client"
      #
      # max_batch_count = (recordset_size / max_batch_size.to_f).ceil
      # puts blue "#{max_batch_count} batches should be processed"

      res = JobProcessing::Client::Sync.new.run(options)
      puts green res.to_s
      # pid = fork do
      # end
      # Process.detach(pid)
      # puts green "Started a client ##{pid}"

      puts blue "Client has finished submitting jobs"
    end
  end  
end
