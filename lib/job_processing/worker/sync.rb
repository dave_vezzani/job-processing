#  job_worker.rb

require "job_processing/version"
require "job_processing/standard_process"
require 'sequel'
require 'active_record'
require 'arel'
require 'squeel'
require 'logger'
require 'mongo'
require 'mongoid'
require 'configatron'
require 'logger'
require 'gearman'

module JobProcessing
  module Worker
    class Sync < JobProcessing::StandardProcess

      DEFAULT_MONGO_URI = 'mongodb://localhost:27017/test'

      def connect_to_mysql
        gem('mysql')
        mysql_connection_uri = config.srcdb do |srcdb|
          #TODO: sanitize configuration values so that a valid uri results
          sprintf('mysql://%s:%s@%s:%s/%s', srcdb.username, srcdb.password, srcdb.host, srcdb.port, srcdb.database)
        end

        db = Sequel.connect(mysql_connection_uri)

        uri_scrubbed = sanitized_db_connection_string(mysql_connection_uri)
        info "connected to mysql using generated uri: #{uri_scrubbed}"
        db
      end

      def connect_to_db(uri)
        #TODO: validate format of uri
        db = Sequel.connect(uri)

        #TODO: scrub out entire password, even passwords with one or more '@' characters
        # uri_scrubbed = (uri.scan(/@/).length > 2) ? "<hidden>" : uri.sub(/(:[^:]+:)([^@]+)@/, '\1XXXXXX@')
        uri_scrubbed = sanitized_db_connection_string(uri)
        info "connected to database using specified uri: #{uri_scrubbed}"
        db
      end

      def connect_to_mongo(uri)
        mongo = Mongo::Client.new(uri)
        uri_scrubbed = sanitized_db_connection_string(uri)
        info "connected to mongo using specified uri: #{uri_scrubbed}"
        mongo
      end

      def sanitized_db_connection_string(uri)
        (uri.scan(/@/).length > 2) ? "<hidden>" : uri.sub(/(:[^:]+:)([^@]+)@/, '\1XXXXXX@')
      end

      def count
        db = config.srcdb.has_key?(:uri) ? connect_to_db(config.srcdb.uri) : connect_to_mysql
        records_to_process = db['select hive_product_id, variants.id, qty, descriptors.description, variant_descriptors.value from variants, products, descriptors, variant_descriptors WHERE variants.product_id = products.id AND variant_descriptors.variant_id = variants.id AND variant_descriptors.descriptor_id = descriptors.id'].count
        db.disconnect

        records_to_process
      end

      def start(options={})
        unless logger.nil?
          Gearman.logger = logger 
          Mongo::Logger.logger = logger
        end

        servers = if(options.has_key?(:job_servers))
          options[:job_servers].split(/,/)
        else
          ['localhost:4730']
        end

        bcount = if(options.has_key?(:bcount))
          options[:bcount]
        else
          1
        end

        w = Gearman::Worker.new(servers)
        # logger = Logger.new(STDOUT)
        # logger.level = Logger::DEBUG

        w.add_ability("sync") do |data,job|
          options = JSON.parse(data)

          if(options.has_key?('bcount'))
            bcount = options['bcount']
          end
            
          db = config.srcdb.has_key?(:uri) ? connect_to_db(config.srcdb.uri) : connect_to_mysql
          mongo = config.mongo.has_key?(:uri) ? connect_to_mongo(config.mongo.uri) : connect_to_mongo(DEFAULT_MONGO_URI)

          Mongoid.load!("config/mongoid.yml", :development)
          customer = "localhost"

          start_time = Time.now
          res = write_records(db, customer, bcount, mongo)
          stop_time = Time.now

          res_count = res.count
          db.disconnect

          {iCount: res_count, time: (stop_time - start_time).seconds.to_f.round(2)}.to_json
        end
        
        # # Add a handler for a "sleep" function that takes a single argument, the
        # # number of seconds to sleep before reporting success.
        # w.add_ability("sleep") do |data,job|
        #  seconds = 10
        #  logger.info "Sleeping for #{seconds} seconds"
        #  (1..seconds.to_i).each do |i|
        #    sleep 1
        #    # Report our progress to the job server every second.
        #    job.report_status(i, seconds)
        #  end
        #  # Report success.
        #  true
        # end
        #
        # w.add_ability("blah") do |data,job|
        #  logger.info "Blah, blah, blah..."
        #  # Report our progress to the job server every second.
        #  job.report_status(1, 1)
        #  # Report success.
        #  true
        # end

        loop { w.work }
      end

      def write_records(db, customer, *args)
        res = db['select hive_product_id, variants.id, qty, descriptors.description, variant_descriptors.value from variants, products, descriptors, variant_descriptors WHERE variants.product_id = products.id AND variant_descriptors.variant_id = variants.id AND variant_descriptors.descriptor_id = descriptors.id'].each do |row|

          Inventory.create(:client => customer, :hive_product_id => row[:hive_product_id],
            :qty => row[:qty], :description => row[:description],
            :value => row[:value], :variant_id => row[:id]) 
        end
      end

      def write_records(db, customer, bcount, mongo)
        batch = nil
        cnt = 0
        res = db['select hive_product_id, variants.id, qty, descriptors.description, variant_descriptors.value from variants, products, descriptors, variant_descriptors WHERE variants.product_id = products.id AND variant_descriptors.variant_id = variants.id AND variant_descriptors.descriptor_id = descriptors.id']

        begin
          res = res.extension(:pagination)
          mongo.use("development")
          coll = mongo.database.collection("job_processing_worker_inventories")

          res.each_page(bcount) do |rows|
            batch = []
            rows.each do |row|
              batch << { insert_one: {:client => customer, :hive_product_id => row[:hive_product_id],
                :qty => row[:qty], :description => row[:description],
                :value => row[:value], :variant_id => row[:id]}}
            end
            # bulk = coll.insert_many(batch_job)
            puts "processing #{batch.length} records (of #{cnt})..."
            coll.bulk_write(batch, {})
            cnt+=batch.length
          end
        rescue Exception => e
          debugger
        end
      end
    end

    class Inventory
      include Mongoid::Document
      field :hive_product_id
      field :qty
      field :description
      field :value
      field :variant_id
      field :client, type: String
    end
    
  end
end
