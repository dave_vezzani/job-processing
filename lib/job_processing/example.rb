require "job_processing/version"
require 'sequel'
require 'active_record'
require 'arel'
require 'squeel'
require 'logger'
require 'mongo'
require 'mongoid'
require 'configatron'
require 'logger'

module JobProcessing
  class Example

    attr_reader :config

    VALID_LOG_LEVELS = %w{debug info warn error fatal unknown}
    DEFAULT_MONGO_URI = 'mongodb://localhost:27017/test'

    def initialize(config = nil)
      if(config.nil?)
        job_processing_conf_file = File.join(Dir.pwd, "config", "configatron", "job_processing.rb")

        @config = if(File.exists?(job_processing_conf_file))
          load job_processing_conf_file
          JobProcessing::Configatron.load
        else
          raise "missing configuration file: #{job_processing_conf_file}; 
you can also provide one in JobProcessing::Example#new"
        end

      else
        @config = config
      end

      @config.logging do |logging|
        @logger       = (logging.has_key? :logger) ? logging.logger : nil

        if(@logger)
          logger_level = (logging.has_key? :level) ? logging.level : :debug
          raise "invalid log level value: #{logger_level}" unless VALID_LOG_LEVELS.include?(logger_level.to_s)

          @logger.level = ::Logger.const_get(logger_level.to_s.upcase)
        end
      end
    end

    def log(msg, level)
      @logger.send(level, msg) unless @logger.nil?
    end

    def debug(msg);     log(msg, :debug);   end
    def info(msg);      log(msg, :info);    end
    def warn(msg);      log(msg, :warn);    end
    def debug(error);   log(msg, :error);   end
    def debug(fatal);   log(msg, :fatal);   end
    def debug(unknown); log(msg, :unknown); end

    def connect_to_mysql
      gem('mysql')
      mysql_connection_uri = config.srcdb do |srcdb|
        #TODO: sanitize configuration values so that a valid uri results
        sprintf('mysql://%s:%s@%s:%s/%s', srcdb.username, srcdb.password, srcdb.host, srcdb.port, srcdb.database)
      end

      db = Sequel.connect(mysql_connection_uri)

      uri_scrubbed = sanitized_db_connection_string(mysql_connection_uri)
      info "connected to mysql using generated uri: #{uri_scrubbed}"
      db
    end

    def connect_to_db(uri)
      #TODO: validate format of uri
      db = Sequel.connect(uri)

      #TODO: scrub out entire password, even passwords with one or more '@' characters
      # uri_scrubbed = (uri.scan(/@/).length > 2) ? "<hidden>" : uri.sub(/(:[^:]+:)([^@]+)@/, '\1XXXXXX@')
      uri_scrubbed = sanitized_db_connection_string(uri)
      info "connected to database using specified uri: #{uri_scrubbed}"
      db
    end

    def connect_to_mongo(uri)
      mongo = Mongo::Client.new(uri)
      uri_scrubbed = sanitized_db_connection_string(uri)
      info "connected to mongo using specified uri: #{uri_scrubbed}"
    end

    def sanitized_db_connection_string(uri)
      (uri.scan(/@/).length > 2) ? "<hidden>" : uri.sub(/(:[^:]+:)([^@]+)@/, '\1XXXXXX@')
    end

    def run
      db = config.srcdb.has_key?(:uri) ? connect_to_db(config.srcdb.uri) : connect_to_mysql
      mongo = config.mongo.has_key?(:uri) ? connect_to_mongo(config.mongo.uri) : connect_to_mongo(DEFAULT_MONGO_URI)

      Mongoid.load!("config/mongoid.yml", :development)

      customer = "localhost"

      # class Product
      #   include Mongoid::Document
      #   field :name, type: String
      #   embeds_many :instruments
      # end
      #
      #
      # class Variant
      #   include Mongoid::Document
      #   field :name, type: String
      #   embedded_in :artist
      # end

      db['select hive_product_id, variants.id, qty, descriptors.description, variant_descriptors.value from variants, products, descriptors, variant_descriptors WHERE variants.product_id = products.id AND variant_descriptors.variant_id = variants.id AND variant_descriptors.descriptor_id = descriptors.id'].each do |row|

        Inventory.create(:client => customer, :hive_product_id => row[:hive_product_id],
          :qty => row[:qty], :description => row[:description],
          :value => row[:value], :variant_id => row[:id]) 
      end
    end
  end

  class Inventory
    include Mongoid::Document
    field :hive_product_id
    field :qty
    field :description
    field :value
    field :variant_id
    field :client, type: String
  end
end
