require "job_processing/standard_process"
require 'gearman'

module JobProcessing
  module Client
    class Sync < JobProcessing::StandardProcess

      def run(options={})
        Gearman.logger = logger unless logger.nil?
        
        servers = if(options.has_key?(:job_servers))
          options[:job_servers].split(/,/)
        else
          ['localhost:4730']
        end
        
        bcount = if(options.has_key?(:bcount))
          options[:bcount]
        else
          1
        end
        
        task = Gearman::Task.new('sync', {bcount: bcount}.to_json)
        
        client = Gearman::Client.new(servers)
        taskset = Gearman::TaskSet.new(client)
        
        dataset = nil

        task.on_complete {|d| 
          dataset = d 
        }

        # task2 = Gearman::Task.new('blah')
        # task2.on_complete {|d| puts "Finished with client for blah: #{d}" }

        taskset.add_task(task)
        # taskset.add_task(task2)
        taskset.wait(100)

        logger.info "client notified that worker is done: #{dataset}"
        return dataset
      end
    end
  end
end

