module JobProcessing
  class StandardProcess
    attr_reader :config, :logger

    VALID_LOG_LEVELS = %w{debug info warn error fatal unknown}
 
    def initialize(config = nil)
      if(config.nil?)
        job_processing_conf_file = File.join(Dir.pwd, "config", "configatron", "job_processing.rb")

        @config = if(File.exists?(job_processing_conf_file))
          load job_processing_conf_file
          JobProcessing::Configatron.load
        else
          raise "missing configuration file: #{job_processing_conf_file}; you can also provide one in JobProcessing::Example#new"
        end

      else
        @config = config
      end

      @config.logging do |logging|
        @logger       = (logging.has_key? :logger) ? logging.logger : nil

        if(@logger)
          logger_level = (logging.has_key? :level) ? logging.level : :debug
          raise "invalid log level value: #{logger_level}" unless VALID_LOG_LEVELS.include?(logger_level.to_s)

          @logger.level = ::Logger.const_get(logger_level.to_s.upcase)
        end
      end
    end

    def log(msg, level)
      @logger.send(level, msg) unless @logger.nil?
    end

    def debug(msg);     log(msg, :debug);   end
    def info(msg);      log(msg, :info);    end
    def warn(msg);      log(msg, :warn);    end
    def debug(error);   log(msg, :error);   end
    def debug(fatal);   log(msg, :fatal);   end
    def debug(unknown); log(msg, :unknown); end
  end
end

